package com.isec.orderbook.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isec.orderbook.entity.OrderBook;
import com.isec.orderbook.service.OrderBookService;


@RestController
public class ISECController {
	
	
	@Autowired
	private OrderBookService orderbookService;
	private static Logger logger = LogManager.getLogger(ISECController.class);
	
	@GetMapping(value="/orderbook")
	public OrderBook getOrderBookDtls(@RequestParam(value="reference") String ref )
	{
		logger.info("inside service layer with reference number"+ref);
		OrderBook orddtls =  orderbookService.viewOrderBook(ref);
		return orddtls;
	}
	

}
