package com.isec.orderbook.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="fod_fo_ordr_dtls")
public class OrderBook {
	
	
	@Column(name="FOD_CLM_MTCH_ACC")
	private String matchAccount;
	@Id
	@Column(name="FOD_ORDR_REF")
	private String orderReference;
	@Column(name="FOD_ORDR_DT")
	private String orderDate;
	@Column(name="FOD_PRDCT_TYPE")
	private String productType;
	@Column(name="FOD_STCK_CD")
	private String stockCode;
	@Column(name="FOD_QTY")
	private Integer quantity;
	@Column(name="FOD_PRC")
	private Double price;
	public String getMatchAccount() {
		return matchAccount;
		
	}
	
	
	public void setMatchAccount(String matchAccount) {
		this.matchAccount = matchAccount;
	}
	public String getOrderReference() {
		return orderReference;
	}
	public void setOrderReference(String orderReference) {
		this.orderReference = orderReference;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	

}
