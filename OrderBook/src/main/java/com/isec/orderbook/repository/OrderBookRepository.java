package com.isec.orderbook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.isec.orderbook.entity.OrderBook;

@Repository
public interface OrderBookRepository extends JpaRepository<OrderBook, String>{

	
}
