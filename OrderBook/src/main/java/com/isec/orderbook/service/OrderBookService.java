package com.isec.orderbook.service;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.isec.orderbook.controller.ISECController;
import com.isec.orderbook.entity.OrderBook;
import com.isec.orderbook.repository.OrderBookRepository;

@Service
public class OrderBookService {

	@Autowired
	private OrderBookRepository orderBookRepository;

	private static Logger logger = LogManager.getLogger(OrderBookService.class);

	public OrderBook viewOrderBook(String orderReference) {
		Optional obj = orderBookRepository.findById(orderReference);
		OrderBook orderbk = null;
		if (obj.isPresent()) {
			orderbk = (OrderBook) obj.get();
		}
		logger.info("fetched match account details of " + orderbk.getMatchAccount());
		return orderbk;
	}

}
